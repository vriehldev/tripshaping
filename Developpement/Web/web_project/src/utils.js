export const GOOGLE_MAPS_APIKEY = "AIzaSyDEVNIIS4W08GaTEYt_OfJ0mfA4gB0mxgI";

export const checkStatus = (res) => {
  if (res.ok) {
    return res;
  } else {
    return res.text().then((msg) => {
      throw new Error(msg);
    });
  }
};

// pour heroku
//export const url_prefix = "https://tripshapingapi.herokuapp.com";

// pour la vm redirigé sur sterne
export const url_prefix = "http://sterne.iutrs.unistra.fr:4321";

// export const url_prefix = "http://localhost:4200";
