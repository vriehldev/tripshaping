const task_label_ctrl = require("../controllers/TaskLabel");
const task_ctrl = require("../controllers/task");
const label_ctrl = require("../controllers/label");
const user_ctrl = require("../controllers/user");


module.exports = [
	/**
	 * @swagger
	 * /task/{task_id}/label:
	 *   get:
	 *     tags:
	 *     - TaskLabel
	 *     summary: Retourne les infos assigné à une tâche dont l'id est passé en paramètre.
	 *     description: Retourne les infos assigné à une tâche dont l'id est passé en paramètre.
	 *     parameters:
	 *      - in: path
	 *        name: task_id
	 *        example: 1
	 *        required: true
	 *        schema:
	 *          type: integer     
	 *           
	 *     responses:
	 *       200:
	 *         description: Retourne les infos assigné à une tâche dont l'id est passé en paramètre.
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 id:
	 *                   type: integer
	 *                   description: Id de la relation
	 *                   example: 10
	 *                 title:
	 *                   type: string
	 *                   description: Titre du label
	 *                   example: Nourriture
	 *                 createdAt:
	 *                   type: string
	 *                   description: Date de création de la relation
	 *                   example: 2022-06-03T15:23:42.282Z
	 *                 updateAt:
	 *                   type: string
	 *                   description: Date de modification de la relation
	 *                   example: 2022-06-03T15:23:42.282Z
	 *                 TravelId:
	 *                   type: integer 
	 *                   description: Id du voyage
	 *                   example: 43
	 *                 TaskLabel:
	 *                   type: object 
	 *                   properties:
	 *                     createdAt:
	 *                       type: string
	 *                       description: Date de création de la relation
	 *                       example: 2022-06-03T15:23:42.282Z
	 *                     updateAt:
	 *                       type: string
	 *                       description: Date de modification de la relation
	 *                       example: 2022-06-03T15:23:42.282Z
	 *                     LabelId:
	 *                       type: integer
	 *                       description: Id du label
	 *                       example: 26
	 *                     TaskId:
	 *                       type: integer
	 *                       description: Id de la tâche
	 *                       example: 22
	 *       404:
	 *         description: Id du membre n'existe pas.
	 *       401:
	 *         description: Token invalide / introuvable.
	 *                      
	 */

	/**
	 * @swagger
	 * /label/{label_id}/task:
	 *   get:
	 *     tags:
	 *     - TaskLabel
	 *     summary: Retourne les infos du label assigné à une tâche dont l'id est passé en paramètre.
	 *     description: Retourne les infos du label assigné à une tâche dont l'id est passé en paramètre.
	 *     parameters:
	 *      - in: path
	 *        name: label_id
	 *        example: 1
	 *        required: true
	 *        schema:
	 *          type: integer     
	 *           
	 *     responses:
	 *       200:
	 *         description: Retourne les infos du label assigné à une tâche dont l'id est passé en paramètre.
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 id:
	 *                   type: integer
	 *                   description: Id du label
	 *                   example: 10
	 *                 title:
	 *                   type: string
	 *                   description: Titre de la tâche
	 *                   example: Acheter un kebab
	 *                 date:
	 *                   type: string
	 *                   description: Date de la tâche au format AAAA-MM-JJ
	 *                   example: 2022-05-18
	 *                 isDone:
	 *                   type: boolean
	 *                   description: Indique si une tâche est fini
	 *                 createdAt:
	 *                   type: string
	 *                   description: Date de création de la tâche
	 *                   example: 2022-05-24T18:34:10.286Z
	 *                 updateAt:
	 *                   type: string
	 *                   description: Date de modification de la tâche
	 *                   example: 2022-05-24T18:34:10.286Z
	 *                 TravelId:
	 *                   type: integer 
	 *                   description: Id du voyage
	 *                   example: 42
	 *                 TaskLabel:
	 *                   type: object 
	 *                   properties:
	 *                     createdAt:
	 *                       type: string
	 *                       description: Date de création de la relation
	 *                       example: 2022-06-03T15:23:42.282Z
	 *                     updateAt:
	 *                       type: string
	 *                       description: Date de modification de la relation
	 *                       example: 2022-06-03T15:23:42.282Z
	 *                     LabelId:
	 *                       type: integer
	 *                       description: Id du label
	 *                       example: 16
	 *                     TaskId:
	 *                       type: integer
	 *                       description: Id de la tâche
	 *                       example: 22
	 *       404:
	 *         description: Id du membre n'existe pas.
	 *       401:
	 *         description: Token invalide / introuvable.
	 *                      
	 */



	{
		url: '/task/:task_id/label',
		method: 'get',
		func: [
			user_ctrl.identify_client,
			task_ctrl.load_by_id,
			task_label_ctrl.get_all_label_by_task_id,
		],
	},
	// useless
	{
		url: '/task/:task_id/label/:label_id',
		method: 'get',
		func: [
			user_ctrl.identify_client,
			label_ctrl.load_by_id,
			task_ctrl.load_by_id,
			task_label_ctrl.get_label_id_by_task_id]
	},
	{
		url: '/label/:label_id/task',
		method: 'get',
		func: [
			user_ctrl.identify_client,
			label_ctrl.load_by_id,
			task_label_ctrl.get_all_task_by_label_id
		]
	},
	//à swagger
	{
		url: '/task/:task_id/label/:label_id',
		method: 'post',
		func: [
			user_ctrl.identify_client,
			label_ctrl.load_by_id,
			task_ctrl.load_by_id,
			task_label_ctrl.add_label_id_by_task_id
		]
	},
	//à swagger
	{
		url: '/task/:task_id/label/:label_id',
		method: 'delete',
		func: [
			user_ctrl.identify_client,
			label_ctrl.load_by_id,
			task_ctrl.load_by_id,
			task_label_ctrl.delete_label_id_by_task_id
		]
	},
];
