const user_ctrl = require("../controllers/user");
const usermember_ctrl = require("../controllers/UserMember");

module.exports = [

	/**
	 * @swagger
	 * /user/{user_id}/members:
	 *   get:
	 *     tags:
	 *     - UserMember
	 *     summary: Retourne un membre dont l'id est passé en paramètre.
	 *     description: Retourne le membre demandé avec l'id passé en paramètre.
	 *     parameters:
	 *      - in: path
	 *        name: user_id
	 *        example: 1
	 *        required: true
	 *        schema:
	 *          type: integer     
	 *           
	 *     responses:
	 *       200:
	 *         description: Retourne un membre dont l'id est passé en paramètre.
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: object
	 *               properties:
	 *                 id:
	 *                   type: integer
	 *                   description: Id du membre
	 *                   example: 10
	 *                 name:
	 *                   type: string
	 *                   description: Nom du membre
	 *                   example: Serkan
	 *                 saveLocation:
	 *                   type: boolean
	 *                   description: Boolean pour sauvegarder la position
	 *                 userLogin:
	 *                   type: string
	 *                   description: Pseudo de l'utilisateur
	 *                   example: sDev
	 *                 balance:
	 *                   type: integer
	 *                   description: Solde du membre
	 *                   example: 123,45
	 *                 createdAt:
	 *                   type: string
	 *                   description: Date de création du membre
	 *                   example: 2022-06-03T15:23:42.282Z
	 *                 updateAt:
	 *                   type: string
	 *                   description: Date de modification du membre
	 *                   example: 2022-06-03T15:23:42.282Z
	 *                 TravelId:
	 *                   type: integer 
	 *                   description: Id du voyage
	 *                   example: 1
	 *                 UserId:
	 *                   type: integer 
	 *                   description: Id de l'utilisateur
	 *                   example: 1
	 *       404:
	 *         description: Id du membre n'existe pas.
	 *       401:
	 *         description: Token inexistant / invalide. 
	 *                      
	 */

	{
		url: '/user/:user_id/members',
		method: 'get',
		func: [
			user_ctrl.identify_client,
			user_ctrl.load_by_id,
			usermember_ctrl.get_all_member_by_user_id
		],
	},
]