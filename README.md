# Acrobatt

- Lien vers l'APK mobile Androïd : https://expo.dev/artifacts/eas/avRtFz1RvE4Aq1ajt7ZakC.apk
- Lien vers notre site web : http://sterne.iutrs.unistra.fr:1453/
- Lien vers notre swagger : http://sterne.iutrs.unistra.fr:4321/trip-shaping-swagger/
- Lien vers la documentation : https://git.unistra.fr/lesshaifes/acrobatt/-/tree/main/Documents
- Lien vers nos fichiers de développement : https://git.unistra.fr/lesshaifes/acrobatt/-/tree/main/Developpement
